package testsGraphique;

import java.awt.event.MouseMotionListener;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.awt.Dimension;
import java.awt.event.MouseEvent;

import javax.swing.*;


public class MaFenetre extends JFrame implements MouseListener, MouseMotionListener{
	
	ArrayList<int[]> inputList;
	int[][] couleur;
	int couleurDepart;
	
	public MaFenetre(){
		
	    this.setTitle("Graphisme avec Swing");
	    this.setSize(400, 200); //Définit sa taille : 400 pixels de large et 100 pixels de haut
	    this.setLocationRelativeTo(null); //Nous demandons maintenant à notre objet de se positionner au centre
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Termine le processus lorsqu'on clique sur la croix rouge
	   
	    couleur = new int [4][4];
	    
	    int i;
	    for (i=0;i<=3;i++){
	    	int j;
	 	    for (j=0;j<=3;j++){
	 	    	couleur[i][j] = (i+j)%4;
	 	    }
	    }
	 	    
	    MonDessin figure = new MonDessin(couleur);
	    figure.setPreferredSize(new Dimension(200, 200));
	    
	    this.add(figure); // ajoute une instance de MonDessin a ma fenetre.
	   
	    figure.addMouseMotionListener(this);
	    figure.addMouseListener(this);
	    
	    this.pack();
	    this.setVisible(true);
	    
	    this.inputList= new ArrayList<int[]>();
	    
	}
	
	
	   public void mouseMoved(MouseEvent e) { // must be implemented!! if not error in extension
	        //System.out.println("(" + e.getX()/50+","+e.getY()/50+")");
	    }
	    
	   
	    public void mouseDragged(MouseEvent e) { // must be implemented!! if not error in extension
	    	System.out.println("DRAGGED (" + e.getX()/50+","+e.getY()/50+")");
	    	
	    	int [] position = new int[2];
	    	position[0]=e.getX()/50; // modulo 50 (la taille d'une case)
	    	position[1]=e.getY()/50; // modulo 50 (la taille d'une case)
	    	inputList.add(position);
	    	
	    }
	    
	    public void AfficheInput(){
	    	String affiche = "";
	    	if (this.inputList != null){
		    	int i;
		    	for (i=0;i<=this.inputList.size()-1;i++){
		    		affiche = affiche + "("+ this.inputList.get(i)[0] +","+ this.inputList.get(i)[1]+") "; 
		    	}
		    	System.out.println(affiche);
	    	}
	    }


	    
		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			System.out.println("Pressed(" + e.getX()/50+","+e.getY()/50+")");
			
			this.couleurDepart = this.couleur[e.getX()/50][e.getY()/50];
	    	
	    	System.out.println("la couleur de depart est: " + this.couleurDepart);
	    	
	    	this.inputList = new ArrayList<int[]>();
	    	
		}	    

	           
	    @Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			System.out.println("Released(" + e.getX()/50+","+e.getY()/50+")");
			
			this.AfficheInput();
			
			int i;
		    for (i=0;i<=this.inputList.size()-1;i++){
		    this.couleur[inputList.get(i)[0]][inputList.get(i)[1]] = this.couleurDepart;
		    }
		    
		   repaint();
			
			
			
		}
	    
	    
	    
	    @Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		
	    @Override
		public void mouseClicked(MouseEvent e) {
	       
	    }



}
