package testsGraphique;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;

public class MonDessin extends JPanel {

	int[][] Couleurs;
	
	public MonDessin(int[][] couleurs){
		this.Couleurs = couleurs;
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		  //Vous verrez cette phrase chaque fois que la méthode sera invoquée
	    System.out.println("Je suis exécutée !"); // Sert notament a montrer les occurences de repaint()
	    
	    int i;
	    for (i=0;i<=3;i++){
	    	int j;
	 	    for (j=0;j<=3;j++){
	 	    	
	 	    	Color[] trad = new Color[5];
	 	    	trad[0]= Color.red;
	 	    	trad[1]= Color.blue;
	 	    	trad[2]= Color.yellow;
	 	    	trad[3]= Color.cyan;
	 	    	trad[4]= Color.black;
	 	    	
			    g.setColor(trad[this.Couleurs[i][j]]);
			    g.fillRect(i*50, j*50, 50, 50);
	 	    }
	    }
	  }  


}
