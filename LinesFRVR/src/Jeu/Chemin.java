package Jeu;

import java.util.ArrayList;

public class Chemin {
	
	private Origine Start;
	private Origine End;	
	private ArrayList<Point> Link;
	
	public Chemin(Origine debut){
		this.Start = debut;		
	}
	
	
	//Getters as follow:
	
	public Origine GetStart() {
		return this.Start;
	}
	
	public Origine GetEnd() {
		return this.End;
	}
	
	public ArrayList<Point> GetLink() {
		return this.Link;
	}
	
	
	// Setters as follow:
	
	public void SetStart(Origine debut) {
		this.Start = debut ;
	}
	
	public void SetEnd(Origine fin) {
		this.End = fin;
	}
	
	public void SetLink( ArrayList<Point> listepts) {
		this.Link = listepts;
	}
	
	
	// Other Methods:

	
	public void AddPoint (Point newPt){
		this.Link.add(newPt);
	}

	
	public void PrintPath(){ // affichage d'un chemin
		String aff = "\n Impression du chemin de couleur " + this.Start.GetCol() + " : \n";
		aff= aff + this.Start.AfficherXy();
		if (this.Link != null){
			int i;
			for (i=0; i<= this.Link.size()-1; i++){
				aff = aff + this.Link.get(i).AfficherXy();
			}
		}
		if (this.End != null){
			aff = aff + this.End.AfficherXy();
		}
		System.out.println(aff);
	}

	
	public void SplitChemin(Case cas){
		// splits current Chemin until "cas", "cas" excluded!
		// -> Attention, cas doit être de la bonne classe, point ou origine, pour le bon fonctionnement de SplitChemin. <-

		if (cas instanceof Origine){
			
			if (cas.GetCol() == this.GetStart().GetCol()){
				this.SetStart(new Origine(cas.GetCol(),cas.Getx(),cas.Gety()));
				this.SetLink(new ArrayList<Point>());
				this.SetEnd(null);
			}
		}
		
		else{
			ArrayList<Point> linkBis = new ArrayList<Point>();
			
			int i =0;
			boolean detect = false;
			if(this.GetLink() != null){
				for (i=0; i<= this.GetLink().size()-1; i++){
					
					if (this.GetLink().get(i).IsEqual(cas)){
						i=this.GetLink().size();
						detect =true;
						
					}
					else{
						linkBis.add(this.GetLink().get(i));
					}
				}
			}
			
			this.SetLink(linkBis);
			if (detect){
				this.SetEnd(null);
			}
		}
	}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
