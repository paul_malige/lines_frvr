package Jeu;

public class Trou extends Case {
	
private int c;
	
	public Trou(int col, int abs, int ord){
	super(abs,ord);
	this.c = col;
	}
	
	
	public int GetCol() {
		return c;
	}
	
	
	public void SetCol(int newColor){
		this.c = newColor;
	}

	
	public String Afficher(){
		String aff;
		aff = " Trou   " + this.c;
		return aff;
	}

	
	public String AfficherXy(){
		String aff;
		aff = " Trou (" + this.Getx() + "," + this.Gety() +")" ;
		return aff;
	}

	
	public int[] Getxy(){
		int[] cord = new int[2];
		cord[0]=this.Getx();
		cord[1]=this.Gety();
		return cord;
	}
	
	
	public boolean IsEqual(Trou tr){
		boolean test = false;
		if((this.Getx() == tr.Getx()) && (this.Gety() == tr.Gety()) && (this.GetCol() == tr.GetCol())){
			test = true;
		}
		return test;
	}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
