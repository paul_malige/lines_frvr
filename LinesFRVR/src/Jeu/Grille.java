package Jeu;

import java.util.ArrayList;


public class Grille {

	private int Size;
	private Case[][] Cas = new Case[Size][Size];

	//Getters
	
	public int GetSize(){
		return this.Size;
	}
	
	public Case[][] GetCas(){
		return this.Cas;
	}
	
	// Setters
	
	public void SetSize(int size){
		this.Size = size;
	}
	
	public void SetCas(Case[][] cas){
		this.Cas = cas;
	}
	
	// Constructeur
	
	public Grille(int Taille){
		
		this.Size = Taille;
		this.Cas = new Case[this.Size][this.Size];

		int i;
		for (i = 0; i <= this.Size-1; i++){
			int j;
			for (j = 0; j <= this.Size-1; j++){
				this.Cas[i][j] = new Case();
				this.Cas[i][j].Setx(i);
				this.Cas[i][j].Sety(j);
			}		
		}		
	}
	
	public Grille( Grille grille) { // Constructeur pour créer des copies de grilles, non utilisé.
		
		this.Size = grille.GetSize();
		this.Cas = new Case[this.Size][this.Size];
		
		int i;
		for (i = 0; i <= this.Size-1; i++){
			int j;
			for (j = 0; j <= this.Size-1; j++){
				
				if (grille.Cas[i][j] instanceof Origine){
					
					this.Cas[i][j] = new Origine(grille.GetCas()[i][j].GetCol(), grille.GetCas()[i][j].Getx(), grille.GetCas()[i][j].Gety());
				}
			}		
		}		
	}

	
	public void SetOrigine(Origine[] StartPts){ // place les origines sur la grille à partir d'une liste d'Origines.
		int i;
		for (i= 0; i <= StartPts.length -1; i++){
			int xPts = StartPts[i].Getxy()[0];
			int yPts = StartPts[i].Getxy()[1];
			this.Cas[xPts][yPts]=StartPts[i];
		}
	}
	
	public void SetHole(Trou[] trous){ //Place les trous sur la grille à partir  d'une liste d'Origines.
		int i;
		for (i= 0; i <= trous.length -1; i++){
			int xPts = trous[i].Getxy()[0];
			int yPts = trous[i].Getxy()[1];
			this.Cas[xPts][yPts]=trous[i];
		}
	}

	
	public void AddPoint(Point[] NextPts){
		int i;
		for (i= 0; i <= NextPts.length -1; i++){
			int xPts = NextPts[i].Getxy()[0];
			int yPts = NextPts[i].Getxy()[1];
			this.Cas[xPts][yPts]=NextPts[i];
		}
	}

	
	public void PrintBoard(){ // Affichage de la grille
		String print;
		print = "\n Impression de la grille : \n \n";
		int i;
		for (i = 0; i <= this.Size-1; i++){
			int j;
			for (j = 0; j <= this.Size-1; j++){
					print = print + this.Cas[i][j].Afficher() + " ";						
				}
			print = print + "\n";		
		}
		System.out.println(print);
	}

	
	public boolean IsMovable(int x, int y){ // Test si l'input liste correspond à un move valable.
		
		// on ne peut pas partir d'une case, seulement d'une origine ou d'un point.
		boolean test = false;
		
		if ((this.Cas[x][y] instanceof Origine) ||(this.Cas[x][y] instanceof Point)){
			test = true;
		}
		
		return test;
	}

	
	public void RemovePath(Chemin path){ //ajoute un chemin
		
		//this method only removes from the current grid what is contained in path.Link, Origins are not removed from the grid.
		if (!(path.GetLink() == null)){
			int i;
			for (i=0; i<=path.GetLink().size()-1; i++){
				this.Cas[path.GetLink().get(i).Getx()][path.GetLink().get(i).Gety()] = new Case(path.GetLink().get(i).Getx(),path.GetLink().get(i).Gety());
			}
		}
	}

	
	public void InsertPath(Chemin path){ //enlève un chemin
		
		//this method only adds from the current grid what is contained in path.Link, Origins are not added to the grid.
		if (!(path.GetLink() == null)){
			int i;
			for (i=0; i<=path.GetLink().size()-1; i++){
				if (this.Cas[path.GetLink().get(i).Getx()][path.GetLink().get(i).Gety()] instanceof Origine){
					throw new java.lang.RuntimeException("La partie Link d'un chemin a essayé de changer une origine");
				}
				else{
					this.Cas[path.GetLink().get(i).Getx()][path.GetLink().get(i).Gety()] = new Point(path.GetLink().get(i).GetCol(),path.GetLink().get(i).Getx(),path.GetLink().get(i).Gety());
				}
			}
		}
	}

	
	public void UpdateGrid(ArrayList<Chemin> currentPaths, ArrayList<Case> inputList){ // méthode principale du programme qui met à jour la grille.
		
	
		if (inputList.size() == 1){ // Cas où inputList = [Origine], il suffit de couper les chemins.
			if (this.IsMovable(inputList.get(0).Getx(),inputList.get(0).Gety())){
				int j;
				for (j=0; j<=currentPaths.size()-1; j++){
					
					this.RemovePath(currentPaths.get(j));
					currentPaths.get(j).SplitChemin(this.Cas[inputList.get(0).Getx()][inputList.get(0).Gety()]); // repère les origines
					this.InsertPath(currentPaths.get(j));
					}
			}
		}
		
	
		if (2<=inputList.size()){ // Cas où inputList de taille supérieure ou égale à 2: il faut couper les chemins ET ralonger.
			if (this.IsMovable(inputList.get(0).Getx(),inputList.get(0).Gety())){
				
				
				// faire la discussion selon que inputList.get(inputLIst.size()-1) est une Origine.		

				
				if (inputList.get(inputList.size()-1) instanceof Origine){ // --> SITUATION A <--
					
					int iStretched = 2000;
					
					int k;
					for (k=0; k<= currentPaths.size()-1; k++){
						
						if (currentPaths.get(k).GetStart().GetCol() == inputList.get(0).GetCol()){
							iStretched = k; // Sauvegarde de l'indice du chemin que l'on rallonge.
						}
					}
					
					// Si on revient sur l'origine de depart:
					if ( (inputList.get(inputList.size()-1) instanceof Origine) && (inputList.get(inputList.size()-1).IsEqual(currentPaths.get(iStretched).GetStart())) ){
						System.out.println("condition verifiee");
						
						this.RemovePath(currentPaths.get(iStretched));
						
						currentPaths.get(iStretched).SetLink(new ArrayList<Point>());
						currentPaths.get(iStretched).SetEnd(null);
						
						this.InsertPath(currentPaths.get(iStretched));
					}
					
					// Sinon:
					else{
						
						int i;
						for (i=0; i<=inputList.size()-2; i++){ // pas besoin de split le dernier car c'est une origine du chemin ralongé.
							int j;
							for (j=0; j<=currentPaths.size()-1; j++){
								
								this.RemovePath(currentPaths.get(j));
								currentPaths.get(j).SplitChemin(this.Cas[inputList.get(i).Getx()][inputList.get(i).Gety()]); // repère les origines
								this.InsertPath(currentPaths.get(j));
								
								if (inputList.get(0).GetCol() == currentPaths.get(j).GetStart().GetCol()){
									
									if (!(inputList.get(i) instanceof Origine)){
										currentPaths.get(j).AddPoint(new Point(inputList.get(0).GetCol(),inputList.get(i).Getx(),inputList.get(i).Gety()));
									}
										
									this.RemovePath(currentPaths.get(j));
									this.InsertPath(currentPaths.get(j));
									}
								}
							}
						
						Origine finChemin = new Origine(inputList.get(inputList.size()-1).GetCol(),inputList.get(inputList.size()-1).Getx(),inputList.get(inputList.size()-1).Gety());
						currentPaths.get(iStretched).SetEnd(finChemin); // on rajoute la fin du chemin.
							
						this.RemovePath(currentPaths.get(iStretched));
						this.InsertPath(currentPaths.get(iStretched));
					}
				}
					
				
				
				
				else {  // --> SITUATION B <--
					
					int i;
					
					for (i=0; i<=inputList.size()-1; i++){
						int j;
						for (j=0; j<=currentPaths.size()-1; j++){
							
							this.RemovePath(currentPaths.get(j));
							currentPaths.get(j).SplitChemin(this.Cas[inputList.get(i).Getx()][inputList.get(i).Gety()]); // repère les origines
							this.InsertPath(currentPaths.get(j));
							
							if (inputList.get(0).GetCol() == currentPaths.get(j).GetStart().GetCol()){
								//iStretched = j; // Sauvegarde de l'indice du chemin que l'on rallonge.
								
								if (!(inputList.get(i) instanceof Origine)){
									currentPaths.get(j).AddPoint(new Point(inputList.get(0).GetCol(),inputList.get(i).Getx(),inputList.get(i).Gety()));
								}
									
								this.RemovePath(currentPaths.get(j));
								this.InsertPath(currentPaths.get(j));
								
								}
							}
						}				
				}				
			}
		}
	}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

			
	}

}
