package Jeu;

public class Case {

	
	private int x;
	private int y;

	//Constructeur
	
	public Case(){	
	}
	
	
	public Case(int abs, int ord) {
		this.x = abs;
		this.y = ord;		
	}
	
// Getters
	
	public int Getx(){
	return this.x;
	}
	
	public int Gety(){
		return this.y;
	}
	
	public int GetCol(){
		return 1000;
	}
	
// Setters:
	
	public void Setx(int newX){
		this.x= newX;
	}
	
	public void Sety(int newY){
		this.y= newY;
	}
	
	
	// Méthodes:
	
	public boolean IsEqual(Case cas){
		boolean test = false;
		if((this.Getx() == cas.Getx()) && (this.Gety() == cas.Gety())){
			test = true;
		}
		return test;
	}
	
	public String Afficher(){
		String aff;
		aff = " Case     ";
		return aff;
	}
	
	public String AfficherXy(){
		String aff;
		aff = " Case (" + this.x + "," + this.y +")" ;
		return aff;
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}
	

}
