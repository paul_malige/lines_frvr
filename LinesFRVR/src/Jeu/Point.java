package Jeu;

public class Point extends Case {
	
private int c;
	
	public Point(int col, int abs, int ord){
	super(abs,ord);
	this.c = col;
	}
	
	
	public int GetCol() {
		return c;
	}
	
	
	public void SetCol(int newColor){
		this.c = newColor;
	}

	
	public String Afficher(){
		String aff;
		aff = " Point   " + this.c;
		return aff;
	}

	
	public String AfficherXy(){
		String aff;
		aff = " Point (" + this.Getx() + "," + this.Gety() +")" ;
		return aff;
	}

	
	public int[] Getxy(){
		int[] cord = new int[2];
		cord[0]=this.Getx();
		cord[1]=this.Gety();
		return cord;
	}
	
	
	public boolean IsEqual(Point pt){
		boolean test = false;
		if((this.Getx() == pt.Getx()) && (this.Gety() == pt.Gety()) && (this.GetCol() == pt.GetCol())){
			test = true;
		}
		return test;
	}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
