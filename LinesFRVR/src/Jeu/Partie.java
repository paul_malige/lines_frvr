package Jeu;

import java.io.File;
import java.util.ArrayList;

import ConvertisseurXML.*;

public class Partie {
	
	private ConvertisseurXML.LibrairieGrilles GridLibrary; // Librairie des Grilles, des Origines et des Trous
	
	private Grille Grid; // Grille courante
	private Origine[] StartPts; // Origines courantes
	private ArrayList<Chemin> PathSet;
	
	private ArrayList<int[]> InputList = new ArrayList<int[]>();

	
	// Constructeur 1:
	
	public Partie(Grille grid, Origine[] startPts){ 
		this.Grid = grid;
		this.StartPts = startPts;
		
		ArrayList<Chemin> pathSet = new ArrayList<Chemin>();
		int i;
		for (i=0; i<= startPts.length-1; i++){
			if (i%2 ==0){ // 2 origines consécutives ne construisent qu'un seul chemin.
				pathSet.add(new Chemin(startPts[i]));
			}
		}
		this.PathSet = pathSet;
		
	}

	
	//Constructeur 2:
	
	public Partie(ConvertisseurXML.LibrairieGrilles gridLibrary){ 
		
		this.GridLibrary = gridLibrary;
		
		
		if (this.GetGridLibrary().GetGrids().size() != 0){
			this.Grid = this.GetGridLibrary().GetGrids().get(0);
		}
		
		if (this.GetGridLibrary().GetOrigins().size() != 0){
			this.StartPts = this.GetGridLibrary().GetOrigins().get(0);
		}
		
		ArrayList<Chemin> pathSet = new ArrayList<Chemin>();
		int i;
		for (i=0; i<= this.GetStartPts().length-1; i++){
			if (i%2 ==0){ // 2 origines consécutives ne construisent qu'un seul chemin.
				pathSet.add(new Chemin(this.GetStartPts()[i]));
			}
		}
		this.PathSet = pathSet;
		
	}
	
	
	// Getters
	
	public Grille GetGrid(){
		return this.Grid;
	}
	
	public Origine[] GetStartPts(){
		return this.StartPts;
	}
	
	public ArrayList<Chemin> GetPathSet(){
		return this.PathSet;
	}
	
	public ArrayList<int[]> GetInputList(){
		return this.InputList;
	}
	
	public ConvertisseurXML.LibrairieGrilles GetGridLibrary(){
		return this.GridLibrary;
	}
	
	
	// Setters
	
	public void SetGrid(Grille grid){
		this.Grid = grid;
	}
	
	public void SetStartPts(Origine[] startPts){
		this.StartPts = startPts;
	}
	
	public void SetPathSet(ArrayList<Chemin> pathSet){
		this.PathSet = pathSet;
	}
	
	public void SetInputList(ArrayList<int[]> inputList){
		this.InputList = inputList;
	}
	
	public void SetGridLibrary(ConvertisseurXML.LibrairieGrilles gridLibrary){
		this.GridLibrary = gridLibrary;
	}
	
	
	// Methodes necessaires au fonctionnement d'une partie:
	

	public boolean IsMoveCorrect(ArrayList<Case> move){ // méthode qui vérifie que le l'input liste conduit bien à un coup autorisé par les règles du jeu
		boolean test = false;
		
		if ((move.get(0) instanceof Point) || (move.get(0) instanceof Origine)){
			
			
				if (2<=move.size()){ //-> SITUATION A <- i.e. un drag sur au moins deux cases.
					
					int nbOrigineTrou = 0; // on regarde si on passe à travers une origine ou un trou.
						if (3<=move.size()){
						int i;
						for (i=1; i<=move.size()-2;i++){           // the last case of the list "Move" can be of type Origine.
							if ( (move.get(i) instanceof Origine) || (move.get(i) instanceof Trou)){
								nbOrigineTrou= nbOrigineTrou + 1;
							}
						}
					}
					
					
					boolean testOrigineEnd = true; // Verifie que dons le cas où l'on finissent par une origine, elle soit de la bonne couleur; 	
					if ( move.get(move.size()-1) instanceof Origine ){
						
						testOrigineEnd = ( move.get(move.size()-1).GetCol() == move.get(0).GetCol());
								
					}
						
					if ((nbOrigineTrou == 0) && testOrigineEnd){ // verifie que l'intérieur de move ne contient pas d'origine 
						test = true;
					}
				}
			
				
			if ( (move.size() == 1) && (move.get(0) instanceof Origine ) ){ // -> SITUATION B <- i.e. un click ou un drag sur une seule case.
				test = true;
			}
			
		}
		return test;
		
	}
	

	

	public ArrayList<Case> IdentifieInput(){ // revoie une inputlist bis dont les éléments ont le bon type
		
		ArrayList<Case> normInputList = new ArrayList<Case>(); // normalised input list.
		
		int i;
		for (i=0; i<= this.InputList.size()-1; i++){
			
			int I = this.InputList.get(i)[0];
			int J = this.InputList.get(i)[1];
			int col = this.Grid.GetCas()[I][J].GetCol();
			
			if (this.Grid.GetCas()[I][J] instanceof Origine){
				normInputList.add(new Origine(col,I,J));
			}
			
			if (this.Grid.GetCas()[I][J] instanceof Trou){
				normInputList.add(new Trou(col,I,J));
			}
			
			if (this.Grid.GetCas()[I][J] instanceof Point){
				normInputList.add(new Point(col,I,J));
			}
			
			if ( !(this.Grid.GetCas()[I][J] instanceof Origine) && !(this.Grid.GetCas()[I][J] instanceof Point)){
				normInputList.add(new Case(I,J));
			}
		}
		
		return normInputList;
		
	}

	
	public boolean TestFin(){ // test de la condition de fin
		boolean test = false;
		
		// Test si toutes les cases sont occupées:
		
		int sum =0;
		int i;
		for (i=0; i<= this.GetGrid().GetSize()-1; i++){
			int j;
			for (j=0; j<= this.GetGrid().GetSize()-1; j++){
				if ( (this.GetGrid().GetCas()[i][j] instanceof Origine) || (this.GetGrid().GetCas()[i][j] instanceof Point) || (this.GetGrid().GetCas()[i][j] instanceof Trou) ){
					sum = sum +1;
				}
			}	
		}
		
		// Test que chaque Origine appartient bien à un chemin:
		int sum2 =0;
		int k;
		for (k=0; k<= this.GetPathSet().size()-1; k++) {
			if (this.GetPathSet().get(k).GetEnd() != null) {
				sum2 = sum2 +2;
			}
		}
		
		return (sum+sum2 == this.GetGrid().GetSize() * this.GetGrid().GetSize() + this.GetStartPts().length);
	}
	
	
	

	// Section Main, historique des principaux tests réalisés:

	
	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub


		// Tests sur l'objet Case:
		
		/*Case case0 = new Case(3,4);
		Case case1 = new Case();
		
		System.out.println(case0.Getx());
		
		System.out.println(case1);
		System.out.println(case1.Getx());
		
		
		Case[] liste0 = new Case[10];
		
		liste0[1]= new Case();
		System.out.println(liste0[1]);
		
		liste0[1].Setx(5);
		System.out.println(liste0[1].Getx());
		
		
		// Tests sur les objets Origine et Point:
		
		Origine start0 = new Origine(0,3,4);
		System.out.println(start0.GetCol());
		System.out.println(start0.Getxy()[0]);
		System.out.println(start0.Getxy()[1]);
		
		Point point0 = new Point(0,3,4);
		System.out.println(point0.GetCol());
		System.out.println(point0.Getxy()[0]);
		System.out.println(point0.Getxy()[1]);
		
		System.out.println(point0.Getxy().length); */
		

		// Tests sur l'objet Grille:
		
		Origine[] startPts0 = new Origine[2];
		startPts0[0] = new Origine(0,0,1);
		startPts0[1] = new Origine(0,2,1);
		
		Point[] addPts0 = new Point[3];
		addPts0[0] = new Point(0,0,2);
		addPts0[1] = new Point(0,1,2);
		addPts0[2] = new Point(0,2,2);
		
		Grille grille1 = new Grille(5);
		grille1.SetOrigine(startPts0);
		grille1.AddPoint(addPts0);
		
		grille1.PrintBoard();		
		
		// Tests sur l'objet Chemin:
		
		Chemin path0 = new Chemin(startPts0[0]);
		
		ArrayList<Point> link0 = new ArrayList<Point>();
		
		path0.SetLink(link0);
		path0.AddPoint(new Point(0,0,2));
		path0.AddPoint(new Point(0,1,2));
		path0.AddPoint(new Point(0,2,2));
		path0.SetEnd(startPts0[1]);
		
		path0.PrintPath();
		
	// Tests sur la fonction "Split Chemin" :
		
		grille1.RemovePath(path0);
		System.out.println("removed path0:");
		grille1.PrintBoard();
		
		path0.SplitChemin(new Point(0,1,2));
		System.out.println("new path0:");
		path0.PrintPath();
		
		grille1.InsertPath(path0);
		System.out.println("new path0 added:");
		grille1.PrintBoard();
		
	// Tests sur la fonction "Update Grille" :
		
		System.out.println("\n Test sur la fonction UpdateGrille:  ");
		
		Origine[] startPts2 = new Origine[4];
		startPts2[0] = new Origine(0,0,0);
		startPts2[1] = new Origine(0,4,1);
		startPts2[2] = new Origine(1,1,3);
		startPts2[3] = new Origine(1,4,4);
		System.out.println("\n Longueur liste origines " + startPts2.length);
		
		Grille grille2 = new Grille(5);
		grille2.SetOrigine(startPts2);	
		grille2.PrintBoard();	
		
		Partie partie0 = new Partie(grille2, startPts2); // nouvelle partie
		
		
		/* ArrayList<Case> move1 = new ArrayList<Case>();
		move1.add(new Origine(0,0,1));
		move1.add(new Case(0,2));
		move1.add(new Case(0,3));
		
		System.out.println("\n -> Grid update <- ");
		partie0.GetGrid().UpdateGrid(pathSet, move1); // update atrribut Grid in partie0
		partie0.GetGrid().PrintBoard();
		partie0.GetPathSet().get(0).PrintPath();
		
		ArrayList<Case> move2 = new ArrayList<Case>();
		move2.add(new Origine(1,4,3));
		move2.add(new Case(4,4));
		move2.add(new Case(3,4));
		move2.add(new Case(2,4));
		move2.add(new Case(1,4));
		move2.add(new Case(0,4));
		move2.add(new Case(0,3));
		
		System.out.println("\n -> Grid update <- ");
		partie0.GetGrid().UpdateGrid(pathSet, move2);
		partie0.GetGrid().PrintBoard();
		partie0.GetPathSet().get(0).PrintPath();
		partie0.GetPathSet().get(1).PrintPath();
		
		ArrayList<Case> move3 = new ArrayList<Case>();
		move3.add(new Point(0,0,2));
		move3.add(new Point(0,1,2));
		move3.add(new Point(0,1,3));
		move3.add(new Point(0,1,4));
		
		System.out.println("\n -> Grid update <- ");
		partie0.GetGrid().UpdateGrid(pathSet, move3);
		partie0.GetGrid().PrintBoard();
		partie0.GetPathSet().get(0).PrintPath();
		partie0.GetPathSet().get(1).PrintPath(); */
		
		// new Affichage.MaFenetre (partie0);
		
		
		// Création d'une "GridLibrary":

		
		ArrayList<Grille> grids0 = new ArrayList<Grille>();  // contruction à la main d'uneliste de grilles avec origines et trous
		ArrayList<Origine[]> origins0 = new ArrayList<Origine[]>();
		ArrayList<Trou[]> holes0 = new ArrayList<Trou[]>();
		
		grids0.add(new Grille(5));
		grids0.add(new Grille(5));
		grids0.add(new Grille(5));
		
		origins0.add(startPts2);
		Origine[] startPts3 = new Origine[6];
		startPts3[0] = new Origine(0,0,0);
		startPts3[1] = new Origine(0,4,0);
		startPts3[2] = new Origine(1,2,2);
		startPts3[3] = new Origine(1,2,3);
		startPts3[4] = new Origine(2,4,3);
		startPts3[5] = new Origine(2,4,2);
		Origine[] startPts4 = new Origine[4];
		startPts4[0] = new Origine(0,0,3);
		startPts4[1] = new Origine(0,0,4);
		startPts4[2] = new Origine(1,2,4);
		startPts4[3] = new Origine(1,4,4);
		origins0.add(startPts3);
		origins0.add(startPts4);
		
		Trou[] trous0 = new Trou[4];
		trous0[0]=new Trou(-1,0,1);
		trous0[1]=new Trou(-1,1,1);
		trous0[2]=new Trou(-1,3,2);
		trous0[3]=new Trou(-1,4,2);
		Trou[] trous1 = new Trou[2];
		trous1[0]=new Trou(-1,1,0);
		trous1[1]=new Trou(-1,0,2);
		Trou[] trous2 = new Trou[0];
		holes0.add(trous0);
		holes0.add(trous1);
		holes0.add(trous2);
		
		
		LibrairieGrilles GridLibrary1 = new LibrairieGrilles(grids0, origins0,  holes0);
		
		Partie partie1 = new Partie(GridLibrary1);
		new Affichage.MaFenetre (partie1);
		
		
		// Chargement d'une grille à partir de sa description en XML:
		
		/*File xmlFile = new File("/Users/Paul/Desktop/XMLResult"); // pour générer une grille au format XML, utiliser "Create XML".
		
		LibrairieGrilles GridLibrary2 = new LibrairieGrilles(xmlFile);
		
		Partie partie2 = new Partie(GridLibrary2);
		new Affichage.MaFenetre (partie2); */
		
		
		
	}
	
	

}
