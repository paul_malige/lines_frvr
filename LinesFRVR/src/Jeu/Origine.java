package Jeu;

public class Origine extends Case {
	
	private int c;

	// Constructeur
	
	public Origine(int col, int abs, int ord){
	super(abs,ord);
	this.c = col;
	}

	// Getters:
	
	public int GetCol() {
		return c;
	}

	
	public String Afficher(){
		String aff;
		aff = " Origine " + this.c;
		return aff;
	}

	
	public String AfficherXy(){
		String aff;
		aff = " Origine (" + this.Getx() + "," + this.Gety() +")" ;
		return aff;
	}

	
	public int[] Getxy(){
		int[] cord = new int[2];
		cord[0]=this.Getx();
		cord[1]=this.Gety();
		return cord;
	}

	
	public boolean IsEqual(Origine ori){ // compare deux objets de type Origine
		boolean test = false;
		if((this.Getx() == ori.Getx()) && (this.Gety() == ori.Gety()) && (this.GetCol() == ori.GetCol())){
			test = true;
		}
		return test;
	}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
