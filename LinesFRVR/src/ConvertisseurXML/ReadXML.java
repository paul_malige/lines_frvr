package ConvertisseurXML;

import Jeu.Origine;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ReadXML {
	
	

	
	public static void main(String[] args) throws Exception { // Prise en main des méthodes d'interfaceentre XML et Java
		// TODO Auto-generated method stub

		
	File xmlFile = new File("/Users/Paul/Desktop/XMLResult");
	
	DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		
	Document document = documentBuilder.parse(xmlFile);
	
	NodeList list = document.getElementsByTagName("Grille");
	
	//System.out.println(list.getLength()-1);
	
	int i;
	for (i=0; i<= list.getLength()-1; i++){
		
		Node node = list.item(i);
		
		if (node.getNodeType() == Node.ELEMENT_NODE){
			
			
			//On récupère la taille de la liste:
			
			Element element = (Element) node;
			System.out.println("Grille numéro " + element.getAttribute("numéro"));
			
			int nbcase;
			nbcase = Integer.parseInt(element.getElementsByTagName("nombreDeCase").item(0).getTextContent()); // récupère la taille de la grille
			
			System.out.println("Nombre de Cases " + element.getElementsByTagName("nombreDeCase").item(0).getTextContent());
			
			
			// On récupère les origines:
			
			NodeList listOrigines = element.getElementsByTagName("Origine");
			
			int j;
			for (j=0; j<= listOrigines.getLength()-1; j++){
				
				Node node2 = listOrigines.item(j);
				
				if (node2.getNodeType() == Node.ELEMENT_NODE){
					Element element2 = (Element) node2;
					
					System.out.println("Origine numéro " + element2.getAttribute("numéro"));
					System.out.println("Coordonnées " + element2.getElementsByTagName("Coordonnées").item(0).getTextContent());
				}
			}

		}
		
		
	}
	
	
	
	
	}
}
