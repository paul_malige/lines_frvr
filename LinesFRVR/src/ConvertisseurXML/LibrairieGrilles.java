package ConvertisseurXML;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import Jeu.*;

public class LibrairieGrilles {

	private ArrayList<Grille> Grids;
	private ArrayList<Origine[]> Origins;
	private ArrayList<Trou[]> Holes;
	
	
	//Getters:
	
	public ArrayList<Grille> GetGrids(){
		return this.Grids;
	}
	
	public ArrayList<Origine[]> GetOrigins(){
		return this.Origins;
	}
	
	public ArrayList<Trou[]> GetHoles(){
		return this.Holes;
	}
	
	//Setters:
	
	public void SetGrids(ArrayList<Grille> grids){
		this.Grids = grids;
	}
	
	public void SetOrigins(ArrayList<Origine[]> origins){
		this.Origins = origins;
	}
	
	public void SetHoles(ArrayList<Trou[]> holes){
		this.Holes = holes;
	}
	
	
	//Constructeur:
	
	public LibrairieGrilles(ArrayList<Grille> grids, ArrayList<Origine[]> origins, ArrayList<Trou[]> holes){ // Premier constructeur à partir d'une liste de grille, d'origines et de trous.
		this.Grids = grids;
		this.Origins =origins;
		this.Holes=holes;
		
		int i;
		for (i=0; i<= this.Grids.size()-1;i++){
			this.Grids.get(i).SetOrigine(this.Origins.get(i));
		}
		
		if (this.Holes.size()!=0){
			int j;
			for (j=0; j<= this.Grids.size()-1;j++){
				this.Grids.get(j).SetHole(this.Holes.get(j));
			}
		}
		
	}
	
	
	public LibrairieGrilles(File xmlFile) throws Exception{ // -> Second constructeur à partir d'un fichier XML <-

		ArrayList<Grille> grids = new ArrayList<Grille>();
		ArrayList<Jeu.Origine[]> origins = new ArrayList<Jeu.Origine[]>();
		ArrayList<Trou[]> holes = new ArrayList<Jeu.Trou[]>();
		
		
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			
		Document document = documentBuilder.parse(xmlFile);
		
		NodeList list = document.getElementsByTagName("Grille");
		int nbDeGrilles = document.getElementsByTagName("Grille").getLength(); // Récupère le nombres de grilles stockées dans le fichier
		//System.out.println("Nombre de grilles " + nbDeGrilles);
		
		
		int i;
		for (i=0; i<= list.getLength()-1; i++){ // pour chaque Grille
				
			Node node = list.item(i);
	
			
			if (node.getNodeType() == Node.ELEMENT_NODE){
				
				
				//On récupère la taille de la liste:
				
				Element element = (Element) node;
				System.out.println("Grille numéro " + element.getAttribute("numéro"));
				
				int nbcase;
				nbcase = Integer.parseInt(element.getElementsByTagName("nombreDeCase").item(0).getTextContent()); // récupère la taille de la grille
				System.out.println("Nombre de Cases " + element.getElementsByTagName("nombreDeCase").item(0).getTextContent());
				
				grids.add(new Grille(nbcase)); // Crée la liste de Grilles
				//System.out.println("impression");
				//grids.get(0).PrintBoard();
		
				
				// On récupère les origines:
				
				NodeList listOrigines = element.getElementsByTagName("Origine");
				
				int nbOrigines = element.getElementsByTagName("Origine").getLength(); // Récupère le nombres d'origines de la grille numéro i
				System.out.println("Nombre d'Origines de la Grille numéro " +i+ ": " + nbOrigines);
				
				Origine[] listOrigins= new Origine[2*nbOrigines]; // Initialise mon tableau d'Origine pour ma grille numéro i
				// A noter que les origines seront classées pa ordre croissant de couleur.
				
				int j;
				for (j=0; j<= listOrigines.getLength()-1; j++){
					
					Node node2 = listOrigines.item(j);
					
					if (node2.getNodeType() == Node.ELEMENT_NODE){
						Element element2 = (Element) node2;
						
						// Crée la première origine de couleur j:
						Origine origine1 = new Origine(Integer.parseInt(element2.getAttribute("numéro")),Integer.parseInt(element2.getElementsByTagName("Coordonnées").item(0).getTextContent().substring(2,3)),Integer.parseInt(element2.getElementsByTagName("Coordonnées").item(0).getTextContent().substring(4,5)));
						listOrigins[2*Integer.parseInt(element2.getAttribute("numéro"))]=origine1;
						
						/*System.out.println("Couleur origine: " + origine1.GetCol());
						System.out.println("x origine: " +origine1.Getxy()[0]);
						System.out.println("y origine: " + origine1.Getxy()[1]);*/
						
						// Crée la deuxième origine de couleur j:
						Origine origine2 = new Origine(Integer.parseInt(element2.getAttribute("numéro")),Integer.parseInt(element2.getElementsByTagName("Coordonnées").item(0).getTextContent().substring(8,9)),Integer.parseInt(element2.getElementsByTagName("Coordonnées").item(0).getTextContent().substring(10,11)));
						listOrigins[2*Integer.parseInt(element2.getAttribute("numéro"))+1]=origine2;
						
						/*System.out.println("Couleur origine: " + origine2.GetCol());
						System.out.println("x origine: " +origine2.Getxy()[0]);
						System.out.println("y origine: " + origine2.Getxy()[1]);*/
						
						System.out.println("Couple d'origines de couleur " + element2.getAttribute("numéro"));
						System.out.println("Coordonnées: " + element2.getElementsByTagName("Coordonnées").item(0).getTextContent());
					}
				}
				
				origins.add(listOrigins);
				
				
				
				// On récupère les trous:
				
				NodeList listTrous = element.getElementsByTagName("Trou");
				
				int nbTrous = element.getElementsByTagName("Trou").getLength(); // Récupère le nombres d'origines de la grille numéro i
				System.out.println("Nombre de Trous de la Grille numéro " +i+ ": " + nbTrous);
				
				Trou[] listHoles= new Trou[nbTrous]; // Initialise mon tableau d'Origine pour ma grille numéro i
				
				int a;
				for (a=0; a<= listTrous.getLength()-1; a++){
					
					Node node2 = listTrous.item(a);
					
					if (node2.getNodeType() == Node.ELEMENT_NODE){
						Element element0 = (Element) node2;
						
						Trou trou = new Trou(-1,Integer.parseInt(element0.getElementsByTagName("Coordonnées").item(0).getTextContent().substring(1,2)),Integer.parseInt(element0.getElementsByTagName("Coordonnées").item(0).getTextContent().substring(3,4)));
						
						listHoles[a]=trou;
					}		
				}

				holes.add(listHoles);
	
			
			} // Fin de la boucle de la grille i.	
		}
		
		
		// Une fois les listes de Grilles, d'origines et de points crées, on génère notre bibliothèque de grilles de la même manière que le constructeur précédent:

		this.Grids = grids;
		this.Origins =origins;
		this.Holes=holes;		
		
		int k; // place les origines
		for (k=0; k<= this.Grids.size()-1;k++){
			this.Grids.get(k).SetOrigine(this.Origins.get(k));
		}
		
		if (this.Holes.size()==this.Grids.size()){ // place les trous
			int l;
			for (l=0; l<= this.Grids.size()-1;l++){
				this.Grids.get(l).SetHole(this.Holes.get(l));
			}
		}
	
	}
	
}
