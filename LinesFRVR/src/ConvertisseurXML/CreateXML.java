package ConvertisseurXML;

import org.w3c.dom.Element;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class CreateXML {

	
	
	public static void main(String[]args) throws Exception{ // Permet d'écrire des descriptions de grilles au format XML
	
	DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		
	Document document = documentBuilder.newDocument();
	
	Element partie = document.createElement("Partie"); // Pour bien avoir un élément racine unique.
	document.appendChild(partie);
	
	//Première Grille:
	
	Element grille0 = document.createElement("Grille");
	
	Attr attr0 = document.createAttribute("numéro");
	attr0.setValue("0");
	grille0.setAttributeNode(attr0);
	
	Element nbDeCases0 = document.createElement("nombreDeCase");
	nbDeCases0.appendChild(document.createTextNode("5"));
	grille0.appendChild(nbDeCases0);
	
	// Couple d'origine de couleur 0
	
	Element origine00 = document.createElement("Origine"); //premiere origine de la premiere grille
	Attr attr00 = document.createAttribute("numéro");
	attr00.setValue("0");
	origine00.setAttributeNode(attr00);
	
	Element coordonnees00 = document.createElement("Coordonnées");
	coordonnees00.appendChild(document.createTextNode("((0,1),(2,3))"));
	origine00.appendChild(coordonnees00);
	
	grille0.appendChild(origine00);
	
	// Couple d'origines de couleur 1
	
	Element origine01 = document.createElement("Origine"); //premiere origine de la premiere grille
	Attr attr01 = document.createAttribute("numéro");
	attr01.setValue("1");
	origine01.setAttributeNode(attr01);
	
	Element coordonnees01 = document.createElement("Coordonnées");
	coordonnees01.appendChild(document.createTextNode("((0,2),(3,3))"));
	origine01.appendChild(coordonnees01);
	
	grille0.appendChild(origine01);
	
	// Trous
	
	Element trou00 = document.createElement("Trou"); // Premier trou
	Attr attrTrou00 = document.createAttribute("numéro");
	attrTrou00.setValue("0");
	trou00.setAttributeNode(attrTrou00);
	
	Element coordonneesTrou00 = document.createElement("Coordonnées");
	coordonneesTrou00.appendChild(document.createTextNode("(2,2)"));
	trou00.appendChild(coordonneesTrou00);
	
	grille0.appendChild(trou00);
	
	
	Element trou01 = document.createElement("Trou"); // Deuxième trou
	Attr attrTrou01 = document.createAttribute("numéro");
	attrTrou01.setValue("1");
	trou01.setAttributeNode(attrTrou01);
	
	Element coordonneesTrou01 = document.createElement("Coordonnées");
	coordonneesTrou01.appendChild(document.createTextNode("(0,3)"));
	trou01.appendChild(coordonneesTrou01);
	
	grille0.appendChild(trou01);
	
	Element trou02 = document.createElement("Trou"); // Troisième trou
	Attr attrTrou02 = document.createAttribute("numéro");
	attrTrou02.setValue("2");
	trou02.setAttributeNode(attrTrou02);
	
	Element coordonneesTrou02 = document.createElement("Coordonnées");
	coordonneesTrou02.appendChild(document.createTextNode("(0,4)"));
	trou02.appendChild(coordonneesTrou02);
	
	grille0.appendChild(trou02);
	
	
	partie.appendChild(grille0);
	
	
	
	//Deuxième Grille
	
	Element grille1 = document.createElement("Grille");
	
	Attr attr1 = document.createAttribute("numéro");
	attr1.setValue("1");
	grille1.setAttributeNode(attr1);
	
	Element nbDeCases1 = document.createElement("nombreDeCase");
	nbDeCases1.appendChild(document.createTextNode("5"));
	grille1.appendChild(nbDeCases1);
	
	
	// Ajout du couple d'origines de couleur 0
	
	Element origine10 = document.createElement("Origine"); //premiere origine de la premiere grille
	Attr attr10 = document.createAttribute("numéro");
	attr10.setValue("0");
	origine10.setAttributeNode(attr10);
	
	Element coordonnees10 = document.createElement("Coordonnées");
	coordonnees10.appendChild(document.createTextNode("((0,1),(3,3))"));
	origine10.appendChild(coordonnees10);
	
	grille1.appendChild(origine10);
	
	
	// Ajout du couple d'origines de couleur 1
	
		Element origine11 = document.createElement("Origine"); //premiere origine de la premiere grille
		Attr attr11 = document.createAttribute("numéro");
		attr11.setValue("1");
		origine11.setAttributeNode(attr11);
		
		Element coordonnees11 = document.createElement("Coordonnées");
		coordonnees11.appendChild(document.createTextNode("((1,3),(3,1))"));
		origine11.appendChild(coordonnees11);
		
		grille1.appendChild(origine11);
	

		
	partie.appendChild(grille1); // Aujoute notre élément racine unique au document.
	
	
	
	TransformerFactory transformerFactory = TransformerFactory.newInstance();
	Transformer transformer = transformerFactory.newTransformer();
	DOMSource source = new DOMSource(document);
	
	StreamResult streamResult = new StreamResult("/Users/Paul/Desktop/XMLResult");
	
	transformer.transform(source,streamResult);
	
	
	}
	
	
}

