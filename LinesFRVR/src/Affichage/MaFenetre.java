package Affichage;
import javax.swing.JFrame;

import Jeu.Chemin;
import Jeu.Origine;

import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

public class MaFenetre extends JFrame implements MouseListener, MouseMotionListener {
	
	Jeu.Partie Game;
	
	
	
	public MaFenetre(Jeu.Partie game){
		
		this.Game = game;
		
	    this.setTitle("Tracer de la Grille");
	    this.setSize(250, 271); //Définit sa taille : 400 pixels de large et 100 pixels de haut
	    this.setLocationRelativeTo(null); //Nous demandons maintenant à notre objet de se positionner au centre
	   
	    MonDessin figure = new MonDessin(this.Game);
	    this.add(figure); // ajoute une instance de MonDessin a ma fenetre.
	    
	    figure.addMouseMotionListener(this);
	    figure.addMouseListener(this);
	    
	    this.setVisible(true);
	    
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Termine le processus lorsqu'on clique sur la croix rouge
	    
	}
	
	
	
	public void mouseMoved(MouseEvent e) { 
        //System.out.println("(" + e.getX()/50+","+e.getY()/50+")");
	}
    
   
	
	public void mouseDragged(MouseEvent e) { 
    	
    	//System.out.println("DRAGGED (" + e.getX()/50+","+e.getY()/50+")");
 
    	
		if( !(this.Game.TestFin()) ){
		
    	
    		int [] position = new int[2];
    	position[0]=e.getY()/50; // modulo 50 (la taille d'une case) // --> ATTENTION CORRESPONDANCE (x,y) -> (I,J) <--
    	position[1]=e.getX()/50; // modulo 50 (la taille d'une case)
    	
    	if (this.Game.GetInputList().size() == 0){ // Since we add a position to InputList when the mouse is pressed, this condition should never be true.
    		this.Game.GetInputList().add(position);
    	}
    	
    	else{
    		
    		if((this.Game.GetInputList().get(this.Game.GetInputList().size()-1)[0] != position[0]) || (this.Game.GetInputList().get(this.Game.GetInputList().size()-1)[1] != position[1])){
    			// vérifie que la case que l'on souhaite ajouter soit bien différente de la précédente
    			
    			boolean appartient =false;
    			int i;
    			for (i=0; i<= this.Game.GetInputList().size()-1; i++){
    				if ( (this.Game.GetInputList().get(i)[0] == position[0]) && (this.Game.GetInputList().get(i)[1] == position[1])){
    					appartient =true;
    				}
    			}
    			
    			
    			//condition qui évite les doublons dans l'input list:
    			if (appartient){
    				
    				while ( (this.Game.GetInputList().get(this.Game.GetInputList().size()-1)[0] != position[0]) || (this.Game.GetInputList().get(this.Game.GetInputList().size()-1)[1] != position[1]) ){
    					this.Game.GetInputList().remove(this.Game.GetInputList().size()-1);
    				}
    			}

    			else{
    				this.Game.GetInputList().add(position);
    			}
    			
    		}
    	}
    		
    	
    	
    	// Pour un affichage en CONTINU:
 
    	
	    	this.AfficheInput();
			
			System.out.println(" Move is " + this.Game.IsMoveCorrect( this.Game.IdentifieInput()) +"."); //  affichage de l'état du move pour vérifictation
			
			if ( this.Game.IsMoveCorrect( this.Game.IdentifieInput() ) ){
				this.Game.GetGrid().UpdateGrid(Game.GetPathSet(), this.Game.IdentifieInput());
			}
			
			
			int i;
			for (i=0; i<=this.Game.GetPathSet().size()-1; i++){ //affichage des chemins pour vérification
				this.Game.GetPathSet().get(i).PrintPath();
			}
			
			repaint();
		
  
		}
		
    	
    }
    

    public void AfficheInput(){ // méthode permettant l'afficage de l'input list
    	String affiche = "";
    	if (this.Game.GetInputList() != null){
	    	int i;
	    	for (i=0;i<=this.Game.GetInputList().size()-1;i++){
	    		affiche = affiche + "("+ this.Game.GetInputList().get(i)[0] +","+ this.Game.GetInputList().get(i)[1]+") "; 
	    	}
	    	System.out.println("\n InputList: " + affiche);
    	}
    }

    

    @Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
    	System.out.println("Pressed(" + e.getX()/50+","+e.getY()/50+")");
    	
    	
    	if( !(this.Game.TestFin()) ){ // n'est lu que quand la grille n'est pas terminée
		
    		
    		
			int [] position = new int[2];
	    	position[0]=e.getY()/50;  // --> ATTENTION CORRESPONDANCE (x,y) -> (I,J) <--
	    	position[1]=e.getX()/50; 
	    	
	    	
    		if (this.Game.GetInputList().size() == 0){
    			this.Game.GetInputList().add(position); // Sets the first element in "InputList".
    		}
    		
    		else{
		    	
		    	boolean appartient =false;
				int i;
				for (i=0; i<= this.Game.GetInputList().size()-1; i++){
					if ( (this.Game.GetInputList().get(i)[0] == position[0]) && (this.Game.GetInputList().get(i)[1] == position[1])){
						appartient =true;
					}
				}
				
				if (appartient){
					
					while ( (this.Game.GetInputList().get(this.Game.GetInputList().size()-1)[0] != position[0]) || (this.Game.GetInputList().get(this.Game.GetInputList().size()-1)[1] != position[1]) ){
						this.Game.GetInputList().remove(this.Game.GetInputList().size()-1);
					}
				}
	
				else{
			   		this.Game.SetInputList(new ArrayList<int[]>()); // Resets InputList.
					this.Game.GetInputList().add(position); // Sets the first element in "InputList".
				}
    	
    		}
    		
    		
    		this.AfficheInput();
			
			System.out.println(" Move is " + this.Game.IsMoveCorrect( this.Game.IdentifieInput()) +".");
			
			if ( this.Game.IsMoveCorrect( this.Game.IdentifieInput() ) ){
				this.Game.GetGrid().UpdateGrid(Game.GetPathSet(), this.Game.IdentifieInput());
			}
			
			int i;
			for (i=0; i<=this.Game.GetPathSet().size()-1; i++){
				this.Game.GetPathSet().get(i).PrintPath();
			}
			
			repaint();
			
	    	
    	}

    	
	}	    

           
    @Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
    	System.out.println("\n Released(" + e.getX()/50+","+e.getY()/50+")");		

    	
    	// Pour un affichage au COUP PAR COUP:
    	/*
    	if( !(this.Game.TestFin()) ){ // n'est lu que quand la grille n'est pas terminée
    	
    	
    		this.AfficheInput();
		
			System.out.println(" Move is " + this.Game.IsMoveCorrect( this.Game.IdentifieInput()) +".");
			
			if ( this.Game.IsMoveCorrect( this.Game.IdentifieInput() ) ){
				this.Game.GetGrid().UpdateGrid(Game.GetPathSet(), this.Game.IdentifieInput());
			}
			
			int i;
			for (i=0; i<=this.Game.GetPathSet().size()-1; i++){
				this.Game.GetPathSet().get(i).PrintPath();
			}
			
			repaint();
			
			System.out.println("\n Le jeu est-il terminé? " + this.Game.TestFin());
		
    	
    	}*/
		
	}
    
    
    
    @Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	int niveau = 0;
	
    
	
	@Override
	public void mouseClicked(MouseEvent e) {
    	
    	
		if( this.Game.TestFin() ){ //Charge une autre Grille quand la partie est terminée.
 
    		niveau = (niveau +1) % this.Game.GetGridLibrary().GetGrids().size(); // les niveaux bouclent!
    		
    		System.out.println("le niveau est: " + niveau);
    		
    		/*
    		Jeu.Grille grid = new Jeu.Grille( this.Game.GetGridLibrary().GetGrids().get(niveau) ); // crée un duplicata de la grille numero niveau de la librairie de grilles.
    		Jeu.Origine[] startPts = new Jeu.Origine[this.Game.GetGridLibrary().GetOrigins().get(niveau).length];
    		
    		this.Game = new Jeu.Partie(grid, startPts);
    		*/
    		
    		this.Game.SetGrid( this.Game.GetGridLibrary().GetGrids().get(niveau) );
    		this.Game.SetStartPts( this.Game.GetGridLibrary().GetOrigins().get(niveau) );
    		
    		ArrayList<Chemin> pathSet = new ArrayList<Chemin>();
    		
    		int i;
    		for (i=0; i<= this.Game.GetGridLibrary().GetOrigins().get(niveau).length-1; i++){
    			if (i%2 ==0){ // 2 origines consécutives ne construisent qu'un seul chemin.
    				pathSet.add(new Chemin(this.Game.GetGridLibrary().GetOrigins().get(niveau)[i]));
    			}
    		}
    		
    		this.Game.SetPathSet(pathSet); 
    		
    		repaint();
    		
    	}
    	
    	
    }

}
