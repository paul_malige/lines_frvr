package Affichage;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;

public class MonDessin extends JPanel {
	
	Jeu.Partie Game;
	
	
	public MonDessin(Jeu.Partie game){
		this.Game = game;
	}
	
	@Override
	public void paintComponent(Graphics g){  // Dessin de la Grille entière
		super.paintComponent(g);
	    
		//System.out.println("Je suis exécutée !"); // Check a chaque fois que la methode est executee.
	    
	    int i;
	    for (i=0;i<=this.Game.GetGrid().GetSize()-1;i++){
	    	int j;
	 	    for (j=0;j<=this.Game.GetGrid().GetSize()-1;j++){
	 	    	
	 	    	Color[] trad = new Color[6];
	 	    	trad[0]= new Color(230,102,102);
	 	    	trad[1]= new Color(51,153,255);
	 	    	trad[2]= new Color(0,204,0);
	 	    	trad[3]= new Color(255, 204,0);
	 	    	trad[4]= new Color(153,102,0);
	 	    	trad[5]= new Color(255, 153, 0);
	 	    	
	 	    	Color couleurAffichee;
	 	    	
	 	    	if (this.Game.GetGrid().GetCas()[i][j].GetCol() == -1){
	 	    		couleurAffichee = Color.gray;
	 	    	}
	 	    	else{
		 	    	if (this.Game.GetGrid().GetCas()[i][j].GetCol() == 1000){
		 	    		couleurAffichee = Color.white;
		 	    	}
		 	    	else{
		 	    		couleurAffichee = trad[this.Game.GetGrid().GetCas()[i][j].GetCol()];
		 	    	}
	 	    	}
	 	    	
			    g.setColor(couleurAffichee);
			    g.fillRect(this.Game.GetGrid().GetCas()[i][j].Gety()*50, this.Game.GetGrid().GetCas()[i][j].Getx()*50, 50, 50); // transformation (ligne,colonne) en (colonne,ligne)
			    
			    if (this.Game.GetGrid().GetCas()[i][j] instanceof Jeu.Origine){
			    	g.setColor(Color.black);
			    	g.drawLine(this.Game.GetGrid().GetCas()[i][j].Gety()*50, this.Game.GetGrid().GetCas()[i][j].Getx()*50, (this.Game.GetGrid().GetCas()[i][j].Gety()+1)*50, (this.Game.GetGrid().GetCas()[i][j].Getx()+1)*50);
			    	g.drawLine((this.Game.GetGrid().GetCas()[i][j].Gety()+1)*50, this.Game.GetGrid().GetCas()[i][j].Getx()*50, this.Game.GetGrid().GetCas()[i][j].Gety()*50, (this.Game.GetGrid().GetCas()[i][j].Getx()+1)*50);
			    }
			    
			    g.setColor(Color.black); // Draws the grid.
			    g.drawLine(this.Game.GetGrid().GetCas()[i][j].Gety()*50, this.Game.GetGrid().GetCas()[i][j].Getx()*50, (this.Game.GetGrid().GetCas()[i][j].Gety()+1)*50, this.Game.GetGrid().GetCas()[i][j].Getx()*50);
			    g.drawLine(this.Game.GetGrid().GetCas()[i][j].Gety()*50, this.Game.GetGrid().GetCas()[i][j].Getx()*50, this.Game.GetGrid().GetCas()[i][j].Gety()*50, (this.Game.GetGrid().GetCas()[i][j].Getx()+1)*50);

	 	    }
	    }
	    
	    if(this.Game.TestFin()){ // AFFICHER une image "Well Played".
	    	Font font = new Font("Courier", Font.BOLD, 30); 
	        g.setFont(font);
	    	g.setColor(Color.white);
	    	g.drawString("Partie", 60, 85);		// Very slow to draw String!!
	    	g.drawString("terminée!", 60, 140); 

	    }
	    
	  } 


}
