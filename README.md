# lines_FRVR

The lines_FRVR project is part of the course _"Projet de développement logiciel"_ in the second year of the Supélec program.

# Tasks

The aim is to reproduce a simple flash game by rebuilding it from scratch in Java.

- The original game can be found [here](https://lines.frvr.com).

- We were free to design our own architecture, from class diagram to implementation.
