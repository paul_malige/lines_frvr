package mouseEventTest;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.GridLayout;

import javax.swing.*;

public class MyMouseMotion extends JPanel implements MouseMotionListener {

    JTextArea textArea;
    
    
    static final String NEWLINE = System.getProperty("line.separator");

	
	public MyMouseMotion() {
        
		super();
        
        MonDessin figure = new MonDessin();
        figure.setPreferredSize(new Dimension(200, 200));
        add(figure);
        
        MonDessin figure2 = new MonDessin();
        figure2.setPreferredSize(new Dimension(200, 200));
        add(figure2);
        
        //Area = new area(Color.red);
       // add(Area);
        
        textArea = new JTextArea();
        textArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(textArea,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setPreferredSize(new Dimension(600, 100));
        
        add(scrollPane);
        
        //Register for mouse events on blankArea and panel.
        figure.addMouseMotionListener(this);
        addMouseMotionListener(this);
        
        setPreferredSize(new Dimension(450, 450));
        setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
    }
    
 
	void eventOutput(String eventDescription, MouseEvent e) {
        textArea.append(eventDescription
                + " (" + e.getX() + "," + e.getY() + ")"
                + " detected on "
                + e.getComponent().getClass().getName()
                + NEWLINE);
        textArea.setCaretPosition(textArea.getDocument().getLength());
    }
    
    public void mouseMoved(MouseEvent e) { // must be implemented!! if not error in extension
        eventOutput("Mouse moved", e);
    }
    
    public void mouseDragged(MouseEvent e) { // must be implemented!! if not error in extension
        eventOutput("Mouse dragged", e);
    }
    
	
	
	
	
}