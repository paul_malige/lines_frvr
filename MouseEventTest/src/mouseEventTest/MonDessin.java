package mouseEventTest;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class MonDessin extends JPanel {

	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		  //Vous verrez cette phrase chaque fois que la méthode sera invoquée
	    System.out.println("Je suis exécutée !"); 
	    
	    int i;
	    for (i=0;i<=3;i++){
	    	int j;
	 	    for (j=0;j<=3;j++){
	 	    	
	 	    	Color[] trad = new Color[4];
	 	    	trad[0]= Color.red;
	 	    	trad[1]= Color.blue;
	 	    	trad[2]= Color.yellow;
	 	    	trad[3]= Color.cyan;
	 	    	
			    g.setColor(trad[(i+j)%4]);
			    g.fillRect(i*50, j*50, 50, 50);
	 	    }
	    }
	  }  


}
