package mouseEventTest;

import javax.swing.JComponent;
import javax.swing.JFrame;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	JFrame frame = new JFrame("Essai mouseListener");
    
	
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    
	JComponent newContentPane = new MyMouseMotion();
    
	newContentPane.setOpaque(true); //content panes must be opaque
    frame.setContentPane(newContentPane);
    
    
    frame.pack();
    frame.setVisible(true);
	}
	

}
